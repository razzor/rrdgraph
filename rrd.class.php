<?php
// 6h(1min), 2d(5min), 2w(30min), 2m(2h), 2Y(24h)
define('RRAAVG', 'RRA:AVERAGE:0.5:1:360 RRA:AVERAGE:0.5:5:288 RRA:AVERAGE:0.5:30:336 RRA:AVERAGE:0.5:120:336 RRA:AVERAGE:0.5:576:365 ');
define('RRAMAX', 'RRA:MAX:0.5:1:360 RRA:MAX:0.5:5:288 RRA:MAX:0.5:30:336 RRA:MAX:0.5:120:336 RRA:MAX:0.5:576:365 ');
define('RRAMIN', 'RRA:MIN:0.5:1:360 RRA:MIN:0.5:5:288 RRA:MIN:0.5:30:336 RRA:MIN:0.5:120:336 RRA:MIN:0.5:576:365 ');

class CpuRRD {
	function update($name, $data) {
	    if (!file_exists(RRDPATH."/{$name}.rrd")) {
		exec(RRDTOOL." create ".RRDPATH."/{$name}.rrd --start now -s 60 ".
			"DS:user:DERIVE:120:0:U ".
			"DS:nice:DERIVE:120:0:U ".
			"DS:system:DERIVE:120:0:U ".
			"DS:idle:DERIVE:120:0:U ".
			"DS:iowait:DERIVE:120:0:U ".
			"DS:irq:DERIVE:120:0:U ".
			"DS:softirq:DERIVE:120:0:U ".
			"DS:overall:DERIVE:120:0:U ".
			RRAAVG.RRAMAX.RRAMIN);

		chmod(RRDPATH."/{$name}.rrd", RRDMOD);
		chgrp(RRDPATH."/{$name}.rrd", RRDGRP);
	    }
	    
	    exec(RRDTOOL." update ".RRDPATH."/{$name}.rrd N:{$data[0]}:{$data[1]}:{$data[2]}:{$data[3]}:{$data[4]}:{$data[5]}:{$data[6]}:".
		($data[0]+$data[1]+$data[2]+$data[4]+$data[5]+$data[6]));
	}

	function graph($name, $start, $mode, $title) {
		header('Content-type: image/x-png');
		if ($mode) {
			passthru(RRDTOOL.' graph - --imgformat PNG --start '.$start.' --end -60 --title "'.$title.'" '.
				'--rigid --base=1000 --height=240 --width=800 --alt-autoscale-max --lower-limit=0 '.
				'--vertical-label="percent" '.
				"DEF:a=".RRDPATH."/{$name}.rrd:user:AVERAGE ".
				"DEF:b=".RRDPATH."/{$name}.rrd:nice:AVERAGE ".
				"DEF:c=".RRDPATH."/{$name}.rrd:system:AVERAGE ".
				"DEF:d=".RRDPATH."/{$name}.rrd:idle:AVERAGE ".
		    		"DEF:e=".RRDPATH."/{$name}.rrd:iowait:AVERAGE ".
				"DEF:f=".RRDPATH."/{$name}.rrd:irq:AVERAGE ".
				"DEF:g=".RRDPATH."/{$name}.rrd:softirq:AVERAGE ".
				'AREA:c#FF0000:"System  " GPRINT:c:LAST:"Current\:%8.2lf %s" GPRINT:c:AVERAGE:"Average\:%8.2lf %s" GPRINT:c:MAX:"Maximum\:%8.2lf %s\n" '.
				'STACK:f#EA8F00:"IRQ     " GPRINT:f:LAST:"Current\:%8.2lf %s" GPRINT:f:AVERAGE:"Average\:%8.2lf %s" GPRINT:f:MAX:"Maximum\:%8.2lf %s\n" '.
				'STACK:g#FFFF00:"Soft-IRQ" GPRINT:g:LAST:"Current\:%8.2lf %s" GPRINT:g:AVERAGE:"Average\:%8.2lf %s" GPRINT:g:MAX:"Maximum\:%8.2lf %s\n" '.
				'STACK:e#0000FF:"IO-Wait " GPRINT:e:LAST:"Current\:%8.2lf %s" GPRINT:e:AVERAGE:"Average\:%8.2lf %s" GPRINT:e:MAX:"Maximum\:%8.2lf %s\n" '.
				'STACK:a#00CFCF:"User    " GPRINT:a:LAST:"Current\:%8.2lf %s" GPRINT:a:AVERAGE:"Average\:%8.2lf %s" GPRINT:a:MAX:"Maximum\:%8.2lf %s\n" '.
				'STACK:b#00CF00:"Nice    " GPRINT:b:LAST:"Current\:%8.2lf %s" GPRINT:b:AVERAGE:"Average\:%8.2lf %s" GPRINT:b:MAX:"Maximum\:%8.2lf %s\n" '.
				'');
		} else {
			passthru(RRDTOOL.' graph - --imgformat PNG --start '.$start.' --end -60 --title "'.$title.'" '.
				'--rigid --base=1000 --height=240 --width=800 --alt-autoscale-max --lower-limit=0 '.
				'--vertical-label="percent" '.
				"DEF:xmin=".RRDPATH."/{$name}.rrd:overall:MIN ".
				"DEF:xavg=".RRDPATH."/{$name}.rrd:overall:AVERAGE ".
				"DEF:xmax=".RRDPATH."/{$name}.rrd:overall:MAX ".
				'CDEF:xmid=xmax,xmin,- '.
				'AREA:xmin STACK:xmid#C0C0FF:"min/max CPU" GPRINT:xmin:MIN:"%6.2lf%s" GPRINT:xmax:MAX:"%6.2lf%s" '.
				'LINE1:xmin#a0a0a0 LINE1:xmax#a0a0a0 LINE1:xavg#000080:"avg CPU" '.
				'GPRINT:xavg:MIN:"min\: %6.2lf%s" GPRINT:xavg:AVERAGE:"avg\: %6.2lf%s" GPRINT:xavg:MAX:"max\: %6.2lf%s\n" ');
		}
	}
}


class ProcRRD {
	function update($name, $proc) {
	    if (!file_exists(RRDPATH."/{$name}.rrd")) {
		exec(RRDTOOL." create ".RRDPATH."/{$name}.rrd --start now -s 60 ".
			"DS:intr:DERIVE:120:0:U ".
			"DS:ctxt:DERIVE:120:0:U ".
			"DS:pgfaults:DERIVE:120:0:U ".
			RRAAVG.RRAMAX.RRAMIN);

		chmod(RRDPATH."/{$name}.rrd", RRDMOD);
		chgrp(RRDPATH."/{$name}.rrd", RRDGRP);
	    }
	    
	    exec(RRDTOOL." update ".RRDPATH."/{$name}.rrd N:{$proc[0]}:{$proc[1]}:{$proc[2]}");
	}

	function graph($name, $start, $title) {
		header('Content-type: image/x-png');
		passthru(RRDTOOL.' graph - --imgformat PNG --start '.$start.' --end -60 --title "'.$title.'" '.
			'--rigid --base=1000 --height=120 --width=800 --alt-autoscale-max --lower-limit=0 '.
			'--vertical-label="per second" '.
			"DEF:a=".RRDPATH."/{$name}.rrd:intr:AVERAGE ".
			"DEF:b=".RRDPATH."/{$name}.rrd:ctxt:AVERAGE ".
			"DEF:c=".RRDPATH."/{$name}.rrd:pgfaults:AVERAGE ".
			'AREA:b#00AF00:"Context   " GPRINT:b:LAST:"Current\:%8.2lf %s" GPRINT:b:AVERAGE:"Average\:%8.2lf %s" GPRINT:b:MAX:"Maximum\:%8.2lf %s\n" '.
			'LINE1:a#FF0000:"Interrupts" GPRINT:a:LAST:"Current\:%8.2lf %s" GPRINT:a:AVERAGE:"Average\:%8.2lf %s" GPRINT:a:MAX:"Maximum\:%8.2lf %s\n" '.
			'LINE1:c#0000FF:"Pagefaults" GPRINT:c:LAST:"Current\:%8.2lf %s" GPRINT:c:AVERAGE:"Average\:%8.2lf %s" GPRINT:c:MAX:"Maximum\:%8.2lf %s\n" '.
			'');
	}
}

class LavgRRD {
	function update($name, $load) {
	    if (!file_exists(RRDPATH."/{$name}.rrd")) {
		exec(RRDTOOL." create ".RRDPATH."/{$name}.rrd --start now -s 60 ".
			"DS:1min:GAUGE:120:0:U ".
			"DS:5min:GAUGE:120:0:U ".
			"DS:15min:GAUGE:120:0:U ".
			RRAAVG.RRAMAX.RRAMIN);

		chmod(RRDPATH."/{$name}.rrd", RRDMOD);
		chgrp(RRDPATH."/{$name}.rrd", RRDGRP);
	    }
	    
	    exec(RRDTOOL." update ".RRDPATH."/{$name}.rrd N:{$load[0]}:{$load[1]}:{$load[2]}");
	}

	function graph($name, $start, $mode, $title) {
		header('Content-type: image/x-png');
		if ($mode) {
    			passthru(RRDTOOL.' graph - --imgformat PNG --start '.$start.' --end -60 --title "'.$title.'" '.
				'--rigid --base=1000 --height=120 --width=800 --alt-autoscale-max --lower-limit=0 '.
				'--vertical-label="load" '.
				"DEF:a=".RRDPATH."/{$name}.rrd:1min:AVERAGE ".
				"DEF:b=".RRDPATH."/{$name}.rrd:5min:AVERAGE ".
				"DEF:c=".RRDPATH."/{$name}.rrd:15min:AVERAGE ".
				'CDEF:x=a,b,c,MAX,MAX '.
				'AREA:a#EACC00:"1 Minute Average " GPRINT:a:LAST:"Current\:%8.2lf %s\n" '.
				'AREA:b#EA8F00:"5 Minute Average " GPRINT:b:LAST:"Current\:%8.2lf %s\n" '.
				'AREA:c#FF0000:"15 Minute Average" GPRINT:c:LAST:"Current\:%8.2lf %s\n" '.
				'LINE1:x#000000:"Total"');
		} else {
    			passthru(RRDTOOL.' graph - --imgformat PNG --start '.$start.' --end -60 --title "'.$title.'" '.
				'--rigid --base=1000 --height=120 --width=800 --alt-autoscale-max --lower-limit=0 '.
				'--vertical-label="load" '.
				"DEF:lmin=".RRDPATH."/{$name}.rrd:1min:MIN ".
				"DEF:lavg=".RRDPATH."/{$name}.rrd:1min:AVERAGE ".
				"DEF:lmax=".RRDPATH."/{$name}.rrd:1min:MAX ".
				'CDEF:lmid=lmax,lmin,- '.
				'AREA:lmin STACK:lmid#FFA0A0:"min/max Load " GPRINT:lmin:MIN:"%6.2lf%s" GPRINT:lmax:MAX:"%6.2lf%s" '.
				'LINE1:lmin#a0a0a0 LINE1:lmax#a0a0a0 LINE1:lavg#FF0000:"avg Load " '.
				'GPRINT:lavg:MIN:"min\: %6.2lf%s" GPRINT:lavg:AVERAGE:"avg\: %6.2lf%s" GPRINT:lavg:MAX:"max\: %6.2lf%s\n" '.
				'');
		}
	}
}

class MemRRD {
	function update($name, $mem) {
	    if (!file_exists(RRDPATH."/{$name}.rrd")) {
		exec(RRDTOOL." create ".RRDPATH."/{$name}.rrd --start now -s 60 ".
			"DS:total:GAUGE:120:0:U ".
			"DS:free:GAUGE:120:0:U ".
			"DS:buffer:GAUGE:120:0:U ".
			"DS:cache:GAUGE:120:0:U ".
			RRAAVG.RRAMAX.RRAMIN);

		chmod(RRDPATH."/{$name}.rrd", RRDMOD);
		chgrp(RRDPATH."/{$name}.rrd", RRDGRP);
	    }
    	    
	    exec(RRDTOOL." update ".RRDPATH."/{$name}.rrd N:{$mem[0]}:{$mem[1]}:{$mem[2]}:{$mem[3]}");
	}

	function graph($name, $start, $title) {
		header('Content-type: image/x-png');
		passthru(RRDTOOL.' graph - --imgformat PNG --start '.$start.' --end -60 --title "'.$title.'" '.
			'--rigid --base=1024 --height=120 --width=800 --alt-autoscale-max --lower-limit=0 '.
			'--vertical-label="kB" '.
			"DEF:aa=".RRDPATH."/{$name}.rrd:total:AVERAGE ".
			"DEF:bb=".RRDPATH."/{$name}.rrd:free:AVERAGE ".
			"DEF:cc=".RRDPATH."/{$name}.rrd:buffer:AVERAGE ".
			"DEF:dd=".RRDPATH."/{$name}.rrd:cache:AVERAGE ".
			'CDEF:a=aa,1024,* '.
			'CDEF:b=bb,1024,* '.
			'CDEF:c=cc,1024,* '.
			'CDEF:d=dd,1024,* '.
			'CDEF:x=aa,bb,cc,dd,+,+,-,1024,* '.
			'LINE1:a#000000:"Total Memory  " GPRINT:a:LAST:"Current\:%8.2lf %s\n" '.
			'AREA:x#FF0000:"Used Memory   " GPRINT:x:LAST:"Current\:%8.2lf %s" GPRINT:x:AVERAGE:"Average\:%8.2lf %s" GPRINT:x:MAX:"Maximum\:%8.2lf %s\n" '.
			'STACK:c#FF7D00:"Buffer Memory " GPRINT:c:LAST:"Current\:%8.2lf %s" GPRINT:c:AVERAGE:"Average\:%8.2lf %s" GPRINT:c:MAX:"Maximum\:%8.2lf %s\n" '.
			'STACK:d#FFC73B:"Cache Memory  " GPRINT:d:LAST:"Current\:%8.2lf %s" GPRINT:d:AVERAGE:"Average\:%8.2lf %s" GPRINT:d:MAX:"Maximum\:%8.2lf %s\n" '.
			'STACK:b#00CF00:"Free Memory   " GPRINT:b:LAST:"Current\:%8.2lf %s" GPRINT:b:AVERAGE:"Average\:%8.2lf %s" GPRINT:b:MAX:"Maximum\:%8.2lf %s\n" ');
	}
}

class SwapRRD {
	function update($name, $swap) {
	    if (!file_exists(RRDPATH."/{$name}.rrd")) {
		exec(RRDTOOL." create ".RRDPATH."/{$name}.rrd --start now -s 60 ".
			"DS:total:GAUGE:120:0:U ".
			"DS:free:GAUGE:120:0:U ".
			RRAAVG.RRAMAX.RRAMIN);

		chmod(RRDPATH."/{$name}.rrd", RRDMOD);
		chgrp(RRDPATH."/{$name}.rrd", RRDGRP);
	    }
	    
	    exec(RRDTOOL." update ".RRDPATH."/{$name}.rrd N:{$swap[0]}:{$swap[1]}");
	}

	function graph($name, $start, $title) {
		header('Content-type: image/x-png');
		passthru(RRDTOOL.' graph - --imgformat PNG --start '.$start.' --end -60 --title "'.$title.'" '.
			'--rigid --base=1024 --height=120 --width=800 --alt-autoscale-max --lower-limit=0 '.
			'--vertical-label="kB" '.
			"DEF:aa=".RRDPATH."/{$name}.rrd:total:AVERAGE ".
			"DEF:bb=".RRDPATH."/{$name}.rrd:free:AVERAGE ".
			'CDEF:a=aa,1024,* '.
			'CDEF:b=bb,1024,* '.
			'CDEF:x=aa,bb,-,1024,* '.
			'LINE1:a#000000:"Total Swap  " GPRINT:a:LAST:"Current\:%8.2lf %s\n" '.
			'AREA:x#FF0000:"Used Swap   " GPRINT:x:LAST:"Current\:%8.2lf %s" GPRINT:x:AVERAGE:"Average\:%8.2lf %s" GPRINT:x:MAX:"Maximum\:%8.2lf %s\n" '.
			'STACK:b#00CF00:"Free Swap   " GPRINT:b:LAST:"Current\:%8.2lf %s" GPRINT:b:AVERAGE:"Average\:%8.2lf %s" GPRINT:b:MAX:"Maximum\:%8.2lf %s\n" ');
	}
}

class IfaceRRD {
	function update($name, $rx, $tx) {
	    if (!file_exists(RRDPATH."/{$name}.rrd")) {
		exec(RRDTOOL." create ".RRDPATH."/{$name}.rrd --start now -s 60 ".
			"DS:traffic-in:DERIVE:120:0:U ".
			"DS:traffic-out:DERIVE:120:0:U ".
			RRAAVG.RRAMAX.RRAMIN);

		chmod(RRDPATH."/{$name}.rrd", RRDMOD);
		chgrp(RRDPATH."/{$name}.rrd", RRDGRP);
	    }
	    
	    exec(RRDTOOL." update ".RRDPATH."/{$name}.rrd N:{$rx}:{$tx}");
	}

	function graph($name, $start, $mode, $title) {
		header('Content-type: image/x-png');
		// "normal mode"
		if ($mode) {
			passthru(RRDTOOL.' graph - --imgformat PNG --start '.$start.' --end -60 --title "'.$title.'" '.
				'--rigid --base=1000 --height=240 --width=800 --alt-autoscale-max '.
				'--vertical-label="bytes per second" '.
				"DEF:iavg=".RRDPATH."/{$name}.rrd:traffic-in:AVERAGE ".
				"DEF:oavg=".RRDPATH."/{$name}.rrd:traffic-out:AVERAGE ".
				'CDEF:oavgn=oavg,-1,* '.
				'HRULE:0#FF0000 '.
				'AREA:iavg#A0FFA0 LINE1:iavg#008000:"Inbound " '.
				'GPRINT:iavg:LAST:"Current\:%8.2lf%s" GPRINT:iavg:AVERAGE:"Average\:%8.2lf%s" GPRINT:iavg:MAX:"Maximum\:%8.2lf%s\n" '.
				'AREA:oavgn#C0C0FF LINE1:oavgn#000080:"Outbound" '.
				'GPRINT:oavg:LAST:"Current\:%8.2lf%s" GPRINT:oavg:AVERAGE:"Average\:%8.2lf%s" GPRINT:oavg:MAX:"Maximum\:%8.2lf%s\n" ');
		} else {
			passthru(RRDTOOL.' graph - --imgformat PNG --start '.$start.' --end -60 --title "'.$title.'" '.
				'--rigid --base=1000 --height=240 --width=800 --alt-autoscale-max '.
				'--vertical-label="bytes per second" '.
				"DEF:imax=".RRDPATH."/{$name}.rrd:traffic-in:MAX ".
				"DEF:iavg=".RRDPATH."/{$name}.rrd:traffic-in:AVERAGE ".
				"DEF:imin=".RRDPATH."/{$name}.rrd:traffic-in:MIN ".
				"DEF:omax=".RRDPATH."/{$name}.rrd:traffic-out:MAX ".
				"DEF:oavg=".RRDPATH."/{$name}.rrd:traffic-out:AVERAGE ".
				"DEF:omin=".RRDPATH."/{$name}.rrd:traffic-out:MIN ".
				'CDEF:omaxn=omax,-1,* CDEF:oavgn=oavg,-1,* CDEF:ominn=omin,-1,* '.
				'CDEF:imid=imax,imin,- CDEF:omid=omaxn,ominn,- '.
				'HRULE:0#FF0000 '.
				'AREA:imin STACK:imid#A0FFA0:"min/max Inbound " GPRINT:imin:MIN:"%6.2lf%s" GPRINT:imax:MAX:"%6.2lf%s" '.
				'LINE1:imin#a0a0a0 LINE1:imax#a0a0a0 LINE1:iavg#008000:"avg Inbound " '.
				'GPRINT:iavg:MIN:"min\: %6.2lf%s" GPRINT:iavg:AVERAGE:"avg\: %6.2lf%s" GPRINT:iavg:MAX:"max\: %6.2lf%s\n" '.
				'AREA:ominn STACK:omid#C0C0FF:"min/max Outbound" GPRINT:omin:MIN:"%6.2lf%s" GPRINT:omax:MAX:"%6.2lf%s" '.
				'LINE1:ominn#a0a0a0 LINE1:omaxn#a0a0a0 LINE1:oavgn#000080:"avg Outbound" '.
				'GPRINT:oavg:MIN:"min\: %6.2lf%s" GPRINT:oavg:AVERAGE:"avg\: %6.2lf%s" GPRINT:oavg:MAX:"max\: %6.2lf%s\n" ');
		}
	}
}

class FsRRD {
	function update($name, $fs) {
	    if (!file_exists(RRDPATH."/{$name}.rrd")) {
		exec(RRDTOOL." create ".RRDPATH."/{$name}.rrd --start now -s 60 ".
			"DS:total:GAUGE:120:0:U ".
			"DS:used:GAUGE:120:0:U ".
			"DS:free:GAUGE:120:0:U ".
			RRAAVG.RRAMAX.RRAMIN);

		chmod(RRDPATH."/{$name}.rrd", RRDMOD);
		chgrp(RRDPATH."/{$name}.rrd", RRDGRP);
	    }

	    exec(RRDTOOL." update ".RRDPATH."/{$name}.rrd N:{$fs[0]}:{$fs[1]}:{$fs[2]}");
	}

	function graph($name, $start, $title) {
		header('Content-type: image/x-png');
		passthru(RRDTOOL.' graph - --imgformat PNG --start '.$start.' --end -60 --title "'.$title.'" '.
			'--rigid --base=1024 --height=120 --width=800 --alt-autoscale-max --lower-limit=0 '.
			'--vertical-label="kB" '.
			"DEF:aa=".RRDPATH."/{$name}.rrd:total:AVERAGE ".
			"DEF:bb=".RRDPATH."/{$name}.rrd:used:AVERAGE ".
			"DEF:cc=".RRDPATH."/{$name}.rrd:free:AVERAGE ".
			'CDEF:a=aa,1024,* '.
			'CDEF:b=bb,1024,* '.
			'CDEF:c=cc,1024,* '.
			'LINE1:a#000000:"Total " GPRINT:a:LAST:"Current\:%8.2lf %s\n" '.
			'AREA:b#FF0000:"Used  " GPRINT:b:LAST:"Current\:%8.2lf %s" GPRINT:b:AVERAGE:"Average\:%8.2lf %s" GPRINT:b:MAX:"Maximum\:%8.2lf %s\n" '.
			'STACK:c#00CF00:"Free  " GPRINT:c:LAST:"Current\:%8.2lf %s" GPRINT:c:AVERAGE:"Average\:%8.2lf %s" GPRINT:c:MAX:"Maximum\:%8.2lf %s\n" ');
	}
}

class MysqlRRD {
	function update($name, $mysql) {
	    if (!file_exists(RRDPATH."/{$name}.rrd")) {
		exec(RRDTOOL." create ".RRDPATH."/{$name}.rrd --start now -s 60 ".
			"DS:querys:DERIVE:120:0:U ".
			"DS:connections:GAUGE:120:0:U ".
			RRAAVG.RRAMAX.RRAMIN);

		chmod(RRDPATH."/{$name}.rrd", RRDMOD);
		chgrp(RRDPATH."/{$name}.rrd", RRDGRP);
	    }

	    exec(RRDTOOL." update ".RRDPATH."/{$name}.rrd N:{$mysql[0]}:{$mysql[1]}");
	}

	function graph($name, $start, $title) {
		header('Content-type: image/x-png');
		passthru(RRDTOOL.' graph - --imgformat PNG --start '.$start.' --end -60 --title "'.$title.'" '.
			'--rigid --base=1000 --height=120 --width=800 --alt-autoscale-max --lower-limit=0 '.
			'--vertical-label="querys per second" '.
			"DEF:a=".RRDPATH."/{$name}.rrd:querys:AVERAGE ".
			"DEF:b=".RRDPATH."/{$name}.rrd:connections:AVERAGE ".
			'AREA:b#00CF00:"Connections " GPRINT:b:LAST:"Current\:%8.2lf %s" GPRINT:b:AVERAGE:"Average\:%8.2lf %s" GPRINT:b:MAX:"Maximum\:%8.2lf %s\n" '.
			'LINE1:a#002A97:"Querys      " GPRINT:a:LAST:"Current\:%8.2lf %s" GPRINT:a:AVERAGE:"Average\:%8.2lf %s" GPRINT:a:MAX:"Maximum\:%8.2lf %s\n" ');
	}
}

class HttpdRRD {
	function update($name, $httpd) {
	    if (!file_exists(RRDPATH."/{$name}.rrd")) {
		exec(RRDTOOL." create ".RRDPATH."/{$name}.rrd --start now -s 60 ".
			"DS:requests:DERIVE:120:0:U ".
			"DS:traffic:DERIVE:120:0:U ".
			"DS:busyworkers:GAUGE:120:0:U ".
			"DS:idleworkers:GAUGE:120:0:U ".
			RRAAVG.RRAMAX.RRAMIN);

		chmod(RRDPATH."/{$name}.rrd", RRDMOD);
		chgrp(RRDPATH."/{$name}.rrd", RRDGRP);
	    }

	    exec(RRDTOOL." update ".RRDPATH."/{$name}.rrd N:{$httpd[0]}:{$httpd[1]}:{$httpd[2]}:{$httpd[3]}");
	}

	function graph($name, $start, $title) {
		header('Content-type: image/x-png');
		passthru(RRDTOOL.' graph - --imgformat PNG --start '.$start.' --end -60 --title "'.$title.'" '.
			'--rigid --base=1000 --height=120 --width=800 --alt-autoscale-max --lower-limit=0 '.
			'--vertical-label="querys per second" '.
			"DEF:a=".RRDPATH."/{$name}.rrd:requests:AVERAGE ".
			"DEF:b=".RRDPATH."/{$name}.rrd:traffic:AVERAGE ".
			"DEF:c=".RRDPATH."/{$name}.rrd:busyworkers:AVERAGE ".
			"DEF:d=".RRDPATH."/{$name}.rrd:idleworkers:AVERAGE ".
			'AREA:d#00CF00:"Idle Workers " GPRINT:d:LAST:"Current\:%8.2lf %s" GPRINT:d:AVERAGE:"Average\:%8.2lf %s" GPRINT:d:MAX:"Maximum\:%8.2lf %s\n" '.
			'STACK:c#FF0000:"Busy Workers " GPRINT:c:LAST:"Current\:%8.2lf %s" GPRINT:c:AVERAGE:"Average\:%8.2lf %s" GPRINT:c:MAX:"Maximum\:%8.2lf %s\n" '.
			'LINE1:a#002A97:"Requests     " GPRINT:a:LAST:"Current\:%8.2lf %s" GPRINT:a:AVERAGE:"Average\:%8.2lf %s" GPRINT:a:MAX:"Maximum\:%8.2lf %s\n" ');
	}
}

class NamedRRD {
	function update($name, $named) {
	    if (!file_exists(RRDPATH."/{$name}.rrd")) {
		exec(RRDTOOL." create ".RRDPATH."/{$name}.rrd --start now -s 60 ".
			"DS:success:DERIVE:120:0:U ".
			"DS:referral:DERIVE:120:0:U ".
			"DS:nxrrset:DERIVE:120:0:U ".
			"DS:nxdomain:DERIVE:120:0:U ".
			"DS:recursion:DERIVE:120:0:U ".
			"DS:failure:DERIVE:120:0:U ".
			RRAAVG.RRAMAX.RRAMIN);

		chmod(RRDPATH."/{$name}.rrd", RRDMOD);
		chgrp(RRDPATH."/{$name}.rrd", RRDGRP);
	    }

	    exec(RRDTOOL." update ".RRDPATH."/{$name}.rrd N:{$named[0]}:{$named[1]}:{$named[2]}:{$named[3]}:{$named[4]}:{$named[5]}");
	}

	function graph($name, $start, $title) {
		header('Content-type: image/x-png');
		passthru(RRDTOOL.' graph - --imgformat PNG --start '.$start.' --end -60 --title "'.$title.'" '.
			'--rigid --base=1000 --height=240 --width=800 --alt-autoscale-max '.
			'--vertical-label="querys per minute" '.
			"DEF:a=".RRDPATH."/{$name}.rrd:success:AVERAGE ".
			"DEF:b=".RRDPATH."/{$name}.rrd:referral:AVERAGE ".
			"DEF:c=".RRDPATH."/{$name}.rrd:nxrrset:AVERAGE ".
			"DEF:d=".RRDPATH."/{$name}.rrd:nxdomain:AVERAGE ".
			"DEF:e=".RRDPATH."/{$name}.rrd:recursion:AVERAGE ".
			"DEF:f=".RRDPATH."/{$name}.rrd:failure:AVERAGE ".
			'CDEF:nc=c,-1,* '.
			'CDEF:nd=d,-1,* '.
			'CDEF:nf=f,-1,* '.
			'AREA:a#00CF00:"success   " GPRINT:a:LAST:"Current\:%8.2lf %s" GPRINT:a:AVERAGE:"Average\:%8.2lf %s" GPRINT:a:MAX:"Maximum\:%8.2lf %s\n" '.
			'AREA:b#00FFFF:"referral  " GPRINT:b:LAST:"Current\:%8.2lf %s" GPRINT:b:AVERAGE:"Average\:%8.2lf %s" GPRINT:b:MAX:"Maximum\:%8.2lf %s\n" '.
			'STACK:e#0000FF:"recursion " GPRINT:e:LAST:"Current\:%8.2lf %s" GPRINT:e:AVERAGE:"Average\:%8.2lf %s" GPRINT:e:MAX:"Maximum\:%8.2lf %s\n" '.
			'HRULE:0#000000 '.
			'AREA:nf#000000:"failure   " GPRINT:f:LAST:"Current\:%8.2lf %s" GPRINT:f:AVERAGE:"Average\:%8.2lf %s" GPRINT:f:MAX:"Maximum\:%8.2lf %s\n" '.
			'STACK:nd#FF0000:"nxdomain  " GPRINT:d:LAST:"Current\:%8.2lf %s" GPRINT:d:AVERAGE:"Average\:%8.2lf %s" GPRINT:d:MAX:"Maximum\:%8.2lf %s\n" '.
			'STACK:nc#EA8F00:"nxrrset   " GPRINT:c:LAST:"Current\:%8.2lf %s" GPRINT:c:AVERAGE:"Average\:%8.2lf %s" GPRINT:c:MAX:"Maximum\:%8.2lf %s\n" '.
			'');
	}
}

class DiskRRD {
	function update($name, $disk) {
	    if (!file_exists(RRDPATH."/{$name}.rrd")) {
		exec(RRDTOOL." create ".RRDPATH."/{$name}.rrd --start now -s 60 ".
			"DS:read:DERIVE:120:0:U ".
			"DS:sec_rd:DERIVE:120:0:U ".
			"DS:write:DERIVE:120:0:U ".
			"DS:sec_wr:DERIVE:120:0:U ".
			RRAAVG.RRAMAX.RRAMIN);

		chmod(RRDPATH."/{$name}.rrd", RRDMOD);
		chgrp(RRDPATH."/{$name}.rrd", RRDGRP);
	    }

	    exec(RRDTOOL." update ".RRDPATH."/{$name}.rrd N:{$disk[0]}:{$disk[1]}:{$disk[2]}:{$disk[3]}");
	}

	function graph($name, $start, $title) {
		header('Content-type: image/x-png');
		passthru(RRDTOOL.' graph - --imgformat PNG --start '.$start.' --end -60 --title "'.$title.'" '.
			'--rigid --base=1000 --height=240 --width=800 --alt-autoscale-max '.
			'--vertical-label="Sektors read/write" '.
			"DEF:r=".RRDPATH."/{$name}.rrd:read:AVERAGE ".
			"DEF:rs=".RRDPATH."/{$name}.rrd:sec_rd:AVERAGE ".
			"DEF:nw=".RRDPATH."/{$name}.rrd:write:AVERAGE ".
			"DEF:nws=".RRDPATH."/{$name}.rrd:sec_wr:AVERAGE ".
			'CDEF:w=nw,-1,* '.
			'CDEF:ws=nws,-1,* '.
			'HRULE:0#FF0000 '.
			'AREA:rs#A0FFA0 LINE1:rs#008000:"Read " LINE1:r#000000 '.
			'GPRINT:rs:LAST:"Current\:%8.2lf%s" GPRINT:rs:AVERAGE:"Average\:%8.2lf%s" GPRINT:rs:MAX:"Maximum\:%8.2lf%s\n" '.
			'AREA:ws#C0C0FF LINE1:ws#000080:"Write" LINE1:w#000000 '.
			'GPRINT:nws:LAST:"Current\:%8.2lf%s" GPRINT:nws:AVERAGE:"Average\:%8.2lf%s" GPRINT:nws:MAX:"Maximum\:%8.2lf%s\n" '.
			'');
	}
}

class UpRRD {
	function update($name, $uptime) {
	    if (!file_exists(RRDPATH."/{$name}.rrd")) {
		exec(RRDTOOL." create ".RRDPATH."/{$name}.rrd --start now -s 60 ".
			"DS:uptime:GAUGE:120:0:U ".
			"DS:idletime:GAUGE:120:0:U ".
			RRAAVG.RRAMAX.RRAMIN);

		chmod(RRDPATH."/{$name}.rrd", RRDMOD);
		chgrp(RRDPATH."/{$name}.rrd", RRDGRP);
	    }
	    
	    exec(RRDTOOL." update ".RRDPATH."/{$name}.rrd N:{$uptime[0]}:{$uptime[1]}");
	}

	function graph($name, $start, $title) {
		header('Content-type: image/x-png');
		passthru(RRDTOOL.' graph - --imgformat PNG --start '.$start.' --end -60 --title "'.$title.'" '.
			'--rigid --base=1000 --height=120 --width=800 --alt-autoscale-max --lower-limit 0 '.
			'--vertical-label="days" '.
			"DEF:ups=".RRDPATH."/{$name}.rrd:uptime:AVERAGE ".
			"DEF:idles=".RRDPATH."/{$name}.rrd:idletime:AVERAGE ".
			'CDEF:up=ups,86400,/ '.
			'CDEF:idle=idles,86400,/ '.
			'AREA:up#00CF00:"Uptime " '.
			'LINE1:idle#002A97:"Idletime " '.
			'');
	}
}

class SquidRRD {
	function update($name, $sq) {
	    if (!file_exists(RRDPATH."/{$name}.rrd")) {
		exec(RRDTOOL." create ".RRDPATH."/{$name}.rrd --start now -s 60 ".
			"DS:requests:DERIVE:120:0:U ".
			"DS:httphits:DERIVE:120:0:U ".
			"DS:httperr:DERIVE:120:0:U ".
			"DS:httpin:DERIVE:120:0:U ".
			"DS:httpout:DERIVE:120:0:U ".
			"DS:srvreq:DERIVE:120:0:U ".
			"DS:srverr:DERIVE:120:0:U ".
			"DS:srvin:DERIVE:120:0:U ".
			"DS:srvout:DERIVE:120:0:U ".
			"DS:cachesize:GAUGE:120:0:U ".
			RRAAVG.RRAMAX.RRAMIN);

		chmod(RRDPATH."/{$name}.rrd", RRDMOD);
		chgrp(RRDPATH."/{$name}.rrd", RRDGRP);
	    }
	    
	    exec(RRDTOOL." update ".RRDPATH."/{$name}.rrd N:{$sq[0]}:{$sq[1]}:{$sq[2]}:{$sq[3]}:{$sq[4]}:{$sq[5]}:{$sq[6]}:{$sq[7]}:{$sq[8]}:{$sq[9]}");
	}

	function graph1($name, $start, $title) {
		header('Content-type: image/x-png');
			passthru(RRDTOOL.' graph - --imgformat PNG --start '.$start.' --end -60 --title "'.$title.'" '.
				'--rigid --base=1000 --height=240 --width=800 --alt-autoscale-max '.
				'--vertical-label="Bytes/s" '.
				"DEF:dd=".RRDPATH."/{$name}.rrd:httpin:AVERAGE ".
		    		"DEF:ee=".RRDPATH."/{$name}.rrd:httpout:AVERAGE ".
				"DEF:hh=".RRDPATH."/{$name}.rrd:srvin:AVERAGE ".
				"DEF:ii=".RRDPATH."/{$name}.rrd:srvout:AVERAGE ".
				"DEF:jj=".RRDPATH."/{$name}.rrd:cachesize:AVERAGE ".
				'CDEF:nd=dd,-1024,* '.
				'CDEF:ne=ee,-1024,* '.
				'CDEF:d=dd,1024,* '.
				'CDEF:e=ee,1024,* '.
				'CDEF:h=hh,1024,* '.
				'CDEF:i=ii,1024,* '.
				'CDEF:j=jj,1024,* '.
				'AREA:h#A0FFA0 LINE1:h#008000:"Server IN " '.
				'GPRINT:h:LAST:"Current\:%8.2lf%s" GPRINT:h:AVERAGE:"Average\:%8.2lf%s" GPRINT:h:MAX:"Maximum\:%8.2lf%s\n" '.
				'AREA:i#C0C0FF LINE1:i#000080:"Server OUT" '.
				'GPRINT:i:LAST:"Current\:%8.2lf%s" GPRINT:i:AVERAGE:"Average\:%8.2lf%s" GPRINT:i:MAX:"Maximum\:%8.2lf%s\n" '.
				'HRULE:0#FF0000:"-\n" '.
				'AREA:ne#A0FFA0 LINE1:ne#008000:"Client OUT" '.
				'GPRINT:e:LAST:"Current\:%8.2lf%s" GPRINT:e:AVERAGE:"Average\:%8.2lf%s" GPRINT:e:MAX:"Maximum\:%8.2lf%s\n" '.
				'AREA:nd#C0C0FF LINE1:nd#000080:"Client IN " '.
				'GPRINT:d:LAST:"Current\:%8.2lf%s" GPRINT:d:AVERAGE:"Average\:%8.2lf%s" GPRINT:d:MAX:"Maximum\:%8.2lf%s\n" '.
				'GPRINT:j:LAST:"Current Cache Size\:%8.2lf%s" '.
				'');
	}

	function graph2($name, $start, $title) {
		header('Content-type: image/x-png');
			passthru(RRDTOOL.' graph - --imgformat PNG --start '.$start.' --end -60 --title "'.$title.'" '.
				'--rigid --base=1000 --height=240 --width=800 --alt-autoscale-max '.
				'--vertical-label="kb/s" '.
				"DEF:a=".RRDPATH."/{$name}.rrd:requests:AVERAGE ".
		    		"DEF:b=".RRDPATH."/{$name}.rrd:httphits:AVERAGE ".
				"DEF:f=".RRDPATH."/{$name}.rrd:srvreq:AVERAGE ".
				'CDEF:na=a,-1,* '.
				'AREA:f#C0C0FF LINE1:f#000080:"Server Requests " '.
				'GPRINT:f:LAST:"Current\:%8.2lf%s" GPRINT:f:AVERAGE:"Average\:%8.2lf%s" GPRINT:f:MAX:"Maximum\:%8.2lf%s\n" '.
				'HRULE:0#FF0000 '.
				'AREA:na#A0FFA0 LINE1:na#008000:"Client Requests " '.
				'GPRINT:a:LAST:"Current\:%8.2lf%s" GPRINT:a:AVERAGE:"Average\:%8.2lf%s" GPRINT:a:MAX:"Maximum\:%8.2lf%s\n" '.
				'LINE1:b#FF0000:"Cache Hits      " '.
				'GPRINT:b:LAST:"Current\:%8.2lf%s" GPRINT:b:AVERAGE:"Average\:%8.2lf%s" GPRINT:b:MAX:"Maximum\:%8.2lf%s\n" '.
				'');

	}

}

class ConntrackRRD {
        function update($name, $ct) {
            if (!file_exists(RRDPATH."/{$name}.rrd")) {
                exec(RRDTOOL." create ".RRDPATH."/{$name}.rrd --start now -s 60 ".
                        "DS:total:GAUGE:120:0:U ".
                        "DS:max:GAUGE:120:0:U ".
                        "DS:assured:GAUGE:120:0:U ".
                        "DS:unreplied:GAUGE:120:0:U ".
                        "DS:tcp:GAUGE:120:0:U ".
                        "DS:udp:GAUGE:120:0:U ".
                        "DS:unknown:GAUGE:120:0:U ".
                        "DS:local:GAUGE:120:0:U ".
                        "DS:nated:GAUGE:120:0:U ".
                        RRAAVG.RRAMAX.RRAMIN);

                chmod(RRDPATH."/{$name}.rrd", RRDMOD);
                chgrp(RRDPATH."/{$name}.rrd", RRDGRP);
            }

            exec(RRDTOOL." update ".RRDPATH."/{$name}.rrd N:{$ct['total']}:{$ct['max']}:{$ct['assured']}:{$ct['unreplied']}:{$ct['tcp']}:{$ct['udp']}:{$ct['unknown']}:{$ct['local']}:{$ct['nated']}");
        }

        function graph1($name, $start, $mode, $title) {
                // "normal mode"
                if ($mode) {
                        header('Content-type: image/x-png');
                        passthru(RRDTOOL.' graph - --imgformat PNG --start '.$start.' --end -60 --title "'.$title.'" '.
                                '--rigid --base=1000 --height=240 --width=800 --alt-autoscale-max '.
                                '--vertical-label="Connections" '.
                                "DEF:tot=".RRDPATH."/{$name}.rrd:total:AVERAGE ".
                                "DEF:max=".RRDPATH."/{$name}.rrd:max:AVERAGE ".
                                "DEF:lavg=".RRDPATH."/{$name}.rrd:local:AVERAGE ".
                                "DEF:navg=".RRDPATH."/{$name}.rrd:nated:AVERAGE ".
                                'CDEF:navgn=navg,-1,* '.
                                'HRULE:0#FF0000 '.
                                'AREA:lavg#A0FFA0 LINE1:lavg#008000:"local    " '.
                                'GPRINT:lavg:LAST:"Current\:%8.0lf" GPRINT:lavg:AVERAGE:"Average\:%8.0lf" GPRINT:lavg:MAX:"Maximum\:%8.0lf\n" '.
                                'AREA:navgn#C0C0FF LINE1:navgn#000080:"forwared " '.
                                'GPRINT:navg:LAST:"Current\:%8.0lf" GPRINT:navg:AVERAGE:"Average\:%8.0lf" GPRINT:navg:MAX:"Maximum\:%8.0lf\n" '.
                                'GPRINT:tot:LAST:"Used\:   %8.0lf \n" GPRINT:max:LAST:"Maximum\:%8.0lf \n" '.
                                '');

                } else {
                        header('Content-type: image/x-png');
                        passthru(RRDTOOL.' graph - --imgformat PNG --start '.$start.' --end -60 --title "'.$title.'" '.
                                '--rigid --base=1000 --height=240 --width=800 --alt-autoscale-max '.
                                '--vertical-label="Connections" '.
                                "DEF:tot=".RRDPATH."/{$name}.rrd:total:AVERAGE ".
                                "DEF:max=".RRDPATH."/{$name}.rrd:max:AVERAGE ".
                                "DEF:lmax=".RRDPATH."/{$name}.rrd:local:MAX ".
                                "DEF:lavg=".RRDPATH."/{$name}.rrd:local:AVERAGE ".
                                "DEF:lmin=".RRDPATH."/{$name}.rrd:local:MIN ".
                                "DEF:nmax=".RRDPATH."/{$name}.rrd:nated:MAX ".
                                "DEF:navg=".RRDPATH."/{$name}.rrd:nated:AVERAGE ".
                                "DEF:nmin=".RRDPATH."/{$name}.rrd:nated:MIN ".
                                'CDEF:nmaxn=nmax,-1,* CDEF:navgn=navg,-1,* CDEF:nminn=nmin,-1,* '.
                                'CDEF:lmid=lmax,lmin,- CDEF:nmid=nmaxn,nminn,- '.
                                'HRULE:0#FF0000 '.
                                'AREA:lmin STACK:lmid#A0FFA0:"min/max local   " GPRINT:lmin:MIN:"%6.0lf" GPRINT:lmax:MAX:"%6.0lf" '.
                                'LINE1:lmin#a0a0a0 LINE1:lmax#a0a0a0 LINE1:lavg#008000:"avg local  " '.
                                'GPRINT:lavg:MIN:"min\: %6.0lf" GPRINT:lavg:AVERAGE:"avg\: %6.0lf" GPRINT:lavg:MAX:"max\: %6.0lf\n" '.
                                'AREA:nminn STACK:nmid#C0C0FF:"min/max forward " GPRINT:nmin:MIN:"%6.0lf" GPRINT:nmax:MAX:"%6.0lf" '.
                                'LINE1:nminn#a0a0a0 LINE1:nmaxn#a0a0a0 LINE1:navgn#000080:"avg forward" '.
                                'GPRINT:navg:MIN:"min\: %6.0lf" GPRINT:navg:AVERAGE:"avg\: %6.0lf" GPRINT:navg:MAX:"max\: %6.0lf\n" '.
                                'GPRINT:tot:LAST:"Used\:   %8.0lf \n" GPRINT:max:LAST:"Maximum\:%8.0lf \n" '.
                                '');
                }
        }

        function graph2($name, $start, $title) {
                header('Content-type: image/x-png');
                        passthru(RRDTOOL.' graph - --imgformat PNG --start '.$start.' --end -60 --title "'.$title.'" '.
                                '--rigid --base=1000 --height=240 --width=800 --alt-autoscale-max '.
                                '--vertical-label="conntracks" '.
                                "DEF:c=".RRDPATH."/{$name}.rrd:assured:AVERAGE ".
                                "DEF:d=".RRDPATH."/{$name}.rrd:unreplied:AVERAGE ".
                                "DEF:e=".RRDPATH."/{$name}.rrd:tcp:AVERAGE ".
                                "DEF:f=".RRDPATH."/{$name}.rrd:udp:AVERAGE ".
                                "DEF:g=".RRDPATH."/{$name}.rrd:unknown:AVERAGE ".
                                'AREA:g#FF7D00:"proto\: unknown " GPRINT:g:LAST:"Current\:%8.0lf" GPRINT:g:AVERAGE:"Average\:%8.0lf" GPRINT:g:MAX:"Maximum\:%8.0lf\n" '.
                                'STACK:f#FFC73B:"proto\: udp     " GPRINT:f:LAST:"Current\:%8.0lf" GPRINT:f:AVERAGE:"Average\:%8.0lf" GPRINT:f:MAX:"Maximum\:%8.0lf\n" '.
                                'STACK:e#00CF00:"proto\: tcp     " GPRINT:e:LAST:"Current\:%8.0lf" GPRINT:e:AVERAGE:"Average\:%8.0lf" GPRINT:e:MAX:"Maximum\:%8.0lf\n" '.
                                'LINE2:c#002A97:"[ASSURED]      " GPRINT:c:LAST:"Current\:%8.0lf" GPRINT:c:AVERAGE:"Average\:%8.0lf" GPRINT:c:MAX:"Maximum\:%8.0lf\n" '.
                                'LINE2:d#FF0000:"[UNREPLIED]    " GPRINT:d:LAST:"Current\:%8.0lf" GPRINT:d:AVERAGE:"Average\:%8.0lf" GPRINT:d:MAX:"Maximum\:%8.0lf\n" '.

                                '');
        }
}

class TcpRRD {
        function update($name, $tcp) {
            if (!file_exists(RRDPATH."/{$name}.rrd")) {
                exec(RRDTOOL." create ".RRDPATH."/{$name}.rrd --start now -s 60 ".
                        "DS:none:GAUGE:120:0:U ".
                        "DS:synsent:GAUGE:120:0:U ".
                        "DS:synrecv:GAUGE:120:0:U ".
                        "DS:established:GAUGE:120:0:U ".
                        "DS:finwait:GAUGE:120:0:U ".
                        "DS:closewait:GAUGE:120:0:U ".
                        "DS:lastack:GAUGE:120:0:U ".
                        "DS:timewait:GAUGE:120:0:U ".
                        "DS:close:GAUGE:120:0:U ".
                        "DS:listen:GAUGE:120:0:U ".
                        RRAAVG.RRAMAX.RRAMIN);

                chmod(RRDPATH."/{$name}.rrd", RRDMOD);
                chgrp(RRDPATH."/{$name}.rrd", RRDGRP);
            }
            exec(RRDTOOL." update ".RRDPATH."/{$name}.rrd N:{$tcp['NONE']}:{$tcp['SYN_SENT']}:{$tcp['SYN_RECV']}:{$tcp['ESTABLISHED']}:{$tcp['FIN_WAIT']}:{$tcp['CLOSE_WAIT']}:{$tcp['LAST_ACK']}:{$tcp['TIME_WAIT']}:{$tcp['CLOSE']}:{$tcp['LISTEN']}");
        }

        function graph($name, $start, $title) {
                header('Content-type: image/x-png');
                        passthru(RRDTOOL.' graph - --imgformat PNG --start '.$start.' --end -60 --title "'.$title.'" '.
                                '--rigid --base=1000 --height=240 --width=800 --alt-autoscale-max '.
                                '--vertical-label="conntracks" '.
                                "DEF:b=".RRDPATH."/{$name}.rrd:synsent:AVERAGE ".
                                "DEF:c=".RRDPATH."/{$name}.rrd:synrecv:AVERAGE ".
                                "DEF:d=".RRDPATH."/{$name}.rrd:established:AVERAGE ".
                                "DEF:e=".RRDPATH."/{$name}.rrd:finwait:AVERAGE ".
                                "DEF:f=".RRDPATH."/{$name}.rrd:closewait:AVERAGE ".
                                "DEF:g=".RRDPATH."/{$name}.rrd:lastack:AVERAGE ".
                                "DEF:h=".RRDPATH."/{$name}.rrd:timewait:AVERAGE ".
                                "DEF:i=".RRDPATH."/{$name}.rrd:close:AVERAGE ".
                                'AREA:b#FF0000:"SYN_SEND   " GPRINT:b:LAST:"Current\:%8.0lf" GPRINT:b:AVERAGE:"Average\:%8.0lf" GPRINT:b:MAX:"Maximum\:%8.0lf\n" '.
                                'STACK:f#FF00FF:"CLOSE_WAIT " GPRINT:f:LAST:"Current\:%8.0lf" GPRINT:f:AVERAGE:"Average\:%8.0lf" GPRINT:f:MAX:"Maximum\:%8.0lf\n" '.
                                'STACK:g#0000FF:"LAST_ACK   " GPRINT:g:LAST:"Current\:%8.0lf" GPRINT:g:AVERAGE:"Average\:%8.0lf" GPRINT:g:MAX:"Maximum\:%8.0lf\n" '.
                                'STACK:h#007FFF:"TIME_WAIT  " GPRINT:h:LAST:"Current\:%8.0lf" GPRINT:h:AVERAGE:"Average\:%8.0lf" GPRINT:h:MAX:"Maximum\:%8.0lf\n" '.
                                'STACK:c#00FFFF:"SYN_RECV   " GPRINT:c:LAST:"Current\:%8.0lf" GPRINT:c:AVERAGE:"Average\:%8.0lf" GPRINT:c:MAX:"Maximum\:%8.0lf\n" '.
                                'STACK:d#00FF00:"ESTABLISHED" GPRINT:d:LAST:"Current\:%8.0lf" GPRINT:d:AVERAGE:"Average\:%8.0lf" GPRINT:d:MAX:"Maximum\:%8.0lf\n" '.
                                'STACK:e#FFFF00:"FIN_WAIT   " GPRINT:e:LAST:"Current\:%8.0lf" GPRINT:e:AVERAGE:"Average\:%8.0lf" GPRINT:e:MAX:"Maximum\:%8.0lf\n" '.
                                'STACK:i#FF7F00:"CLOSE      " GPRINT:i:LAST:"Current\:%8.0lf" GPRINT:i:AVERAGE:"Average\:%8.0lf" GPRINT:i:MAX:"Maximum\:%8.0lf\n" '.
                                '');
        }
}

class RtStatRRD {
        function update($name, $data) {
		if (!file_exists(RRDPATH."/{$name}.rrd")) {
			exec(RRDTOOL." create ".RRDPATH."/{$name}.rrd --start now -s 60 ".
				"DS:entries:GAUGE:120:0:U ".
				"DS:in_hit:DERIVE:120:0:U ".
				"DS:in_slow_tot:DERIVE:120:0:U ".
				"DS:in_slow_mc:DERIVE:120:0:U ".
				"DS:in_no_route:DERIVE:120:0:U ".
				"DS:in_brd:DERIVE:120:0:U ".
				"DS:in_martian_dst:DERIVE:120:0:U ".
				"DS:in_martian_src:DERIVE:120:0:U ".
				"DS:out_hit:DERIVE:120:0:U ".
				"DS:out_slow_tot:DERIVE:120:0:U ".
				"DS:out_slow_mc:DERIVE:120:0:U ".
				"DS:gc_total:DERIVE:120:0:U ".
				"DS:gc_ignored:DERIVE:120:0:U ".
				"DS:gc_goal_miss:DERIVE:120:0:U ".
				"DS:gc_dst_overflow:DERIVE:120:0:U ".
				"DS:in_hlist_search:DERIVE:120:0:U ".
				"DS:out_hlist_search:DERIVE:120:0:U ".
				RRAAVG.RRAMAX.RRAMIN);
			
			chmod(RRDPATH."/{$name}.rrd", RRDMOD);
			chgrp(RRDPATH."/{$name}.rrd", RRDGRP);
		}
		
		$str = 'N';
		foreach($data as $val)
			$str.=':'.$val;
		
		exec(RRDTOOL." update ".RRDPATH."/{$name}.rrd {$str}");
        }

        function graph1($name, $start, $title) {
                header('Content-type: image/x-png');
                        passthru(RRDTOOL.' graph - --imgformat PNG --start '.$start.' --end -60 --title "'.$title.'" '.
                                '--rigid --base=1000 --height=240 --width=800 --alt-autoscale-max '.
                                '--vertical-label="calls" '.
                                "DEF:a=".RRDPATH."/{$name}.rrd:in_hit:AVERAGE ".
                                "DEF:b=".RRDPATH."/{$name}.rrd:in_slow_tot:AVERAGE ".
                                "DEF:c=".RRDPATH."/{$name}.rrd:in_slow_mc:AVERAGE ".
                                "DEF:d=".RRDPATH."/{$name}.rrd:in_no_route:AVERAGE ".
                                "DEF:e=".RRDPATH."/{$name}.rrd:in_brd:AVERAGE ".
                                "DEF:f=".RRDPATH."/{$name}.rrd:in_martian_dst:AVERAGE ".
                                "DEF:g=".RRDPATH."/{$name}.rrd:in_martian_src:AVERAGE ".
                                "DEF:h=".RRDPATH."/{$name}.rrd:in_hlist_search:AVERAGE ".
                                "DEF:i=".RRDPATH."/{$name}.rrd:out_hit:AVERAGE ".
                                "DEF:j=".RRDPATH."/{$name}.rrd:out_slow_tot:AVERAGE ".
                                "DEF:k=".RRDPATH."/{$name}.rrd:out_slow_mc:AVERAGE ".
                                "DEF:l=".RRDPATH."/{$name}.rrd:out_hlist_search:AVERAGE ".
                                'CDEF:ii=i,-1,* '.
                                'CDEF:jj=j,-1,* '.
                                'CDEF:kk=k,-1,* '.
                                'CDEF:ll=l,-1,* '.
                                'LINE1:a#FF0000:"in_hit           " GPRINT:a:LAST:"Current\:%8.0lf" GPRINT:a:AVERAGE:"Average\:%8.0lf" GPRINT:a:MAX:"Maximum\:%8.0lf\n" '.
                                'LINE1:b#FF00FF:"in_slow_tot      " GPRINT:b:LAST:"Current\:%8.0lf" GPRINT:b:AVERAGE:"Average\:%8.0lf" GPRINT:b:MAX:"Maximum\:%8.0lf\n" '.
                                'LINE1:c#0000FF:"in_slow_mc       " GPRINT:c:LAST:"Current\:%8.0lf" GPRINT:c:AVERAGE:"Average\:%8.0lf" GPRINT:c:MAX:"Maximum\:%8.0lf\n" '.
                                'LINE1:d#007FFF:"in_no_route      " GPRINT:d:LAST:"Current\:%8.0lf" GPRINT:d:AVERAGE:"Average\:%8.0lf" GPRINT:d:MAX:"Maximum\:%8.0lf\n" '.
                                'LINE1:e#00FFFF:"in_brd           " GPRINT:e:LAST:"Current\:%8.0lf" GPRINT:e:AVERAGE:"Average\:%8.0lf" GPRINT:e:MAX:"Maximum\:%8.0lf\n" '.
                                'LINE1:f#00FF00:"in_martian_dst   " GPRINT:e:LAST:"Current\:%8.0lf" GPRINT:e:AVERAGE:"Average\:%8.0lf" GPRINT:e:MAX:"Maximum\:%8.0lf\n" '.
                                'LINE1:g#FFFF00:"in_martian_src   " GPRINT:e:LAST:"Current\:%8.0lf" GPRINT:e:AVERAGE:"Average\:%8.0lf" GPRINT:e:MAX:"Maximum\:%8.0lf\n" '.
                                'LINE1:h#FF7F00:"in_hlist_search  " GPRINT:e:LAST:"Current\:%8.0lf" GPRINT:e:AVERAGE:"Average\:%8.0lf" GPRINT:e:MAX:"Maximum\:%8.0lf\n" '.
                                'HRULE:0#000000:"-\n" '.
                                'LINE1:ii#FF0000:"out_hit          " GPRINT:i:LAST:"Current\:%8.0lf" GPRINT:i:AVERAGE:"Average\:%8.0lf" GPRINT:i:MAX:"Maximum\:%8.0lf\n" '.
                                'LINE1:jj#FF00FF:"out_slow_tot     " GPRINT:j:LAST:"Current\:%8.0lf" GPRINT:j:AVERAGE:"Average\:%8.0lf" GPRINT:j:MAX:"Maximum\:%8.0lf\n" '.
                                'LINE1:kk#0000FF:"out_slow_mc      " GPRINT:k:LAST:"Current\:%8.0lf" GPRINT:k:AVERAGE:"Average\:%8.0lf" GPRINT:k:MAX:"Maximum\:%8.0lf\n" '.
                                'LINE1:ll#FF7F00:"out_hlist_search " GPRINT:l:LAST:"Current\:%8.0lf" GPRINT:l:AVERAGE:"Average\:%8.0lf" GPRINT:l:MAX:"Maximum\:%8.0lf\n" '.
                                '');
        }

        function graph2($name, $start, $title) {
                header('Content-type: image/x-png');
                        passthru(RRDTOOL.' graph - --imgformat PNG --start '.$start.' --end -60 --title "'.$title.'" '.
                                '--rigid --base=1000 --height=240 --width=800 --alt-autoscale-max '.
                                '--vertical-label="calls" '.
                                "DEF:a=".RRDPATH."/{$name}.rrd:entries:AVERAGE ".
                                "DEF:b=".RRDPATH."/{$name}.rrd:gc_total:AVERAGE ".
                                "DEF:c=".RRDPATH."/{$name}.rrd:gc_ignored:AVERAGE ".
                                "DEF:d=".RRDPATH."/{$name}.rrd:gc_goal_miss:AVERAGE ".
                                "DEF:e=".RRDPATH."/{$name}.rrd:gc_dst_overflow:AVERAGE ".
                                'LINE1:a#FF0000:"entries         " GPRINT:a:LAST:"Current\:%8.0lf" GPRINT:a:AVERAGE:"Average\:%8.0lf" GPRINT:a:MAX:"Maximum\:%8.0lf\n" '.
                                'LINE1:b#FF00FF:"gc_total        " GPRINT:b:LAST:"Current\:%8.0lf" GPRINT:b:AVERAGE:"Average\:%8.0lf" GPRINT:b:MAX:"Maximum\:%8.0lf\n" '.
                                'LINE1:c#0000FF:"gc_ignored      " GPRINT:c:LAST:"Current\:%8.0lf" GPRINT:c:AVERAGE:"Average\:%8.0lf" GPRINT:c:MAX:"Maximum\:%8.0lf\n" '.
                                'LINE1:d#007FFF:"gc_goal_miss    " GPRINT:d:LAST:"Current\:%8.0lf" GPRINT:d:AVERAGE:"Average\:%8.0lf" GPRINT:d:MAX:"Maximum\:%8.0lf\n" '.
                                'LINE1:e#00FFFF:"gc_dst_overflow " GPRINT:e:LAST:"Current\:%8.0lf" GPRINT:e:AVERAGE:"Average\:%8.0lf" GPRINT:e:MAX:"Maximum\:%8.0lf\n" '.
                                '');
        }
}

class CtStatRRD {
        function update($name, $data) {
		if (!file_exists(RRDPATH."/{$name}.rrd")) {
			exec(RRDTOOL." create ".RRDPATH."/{$name}.rrd --start now -s 60 ".
				"DS:entries:GAUGE:120:0:U ".
				"DS:searched:DERIVE:120:0:U ".
				"DS:found:DERIVE:120:0:U ".
				"DS:new:DERIVE:120:0:U ".
				"DS:invalid:DERIVE:120:0:U ".
				"DS:ignore:DERIVE:120:0:U ".
				"DS:delete:DERIVE:120:0:U ".
				"DS:delete_list:DERIVE:120:0:U ".
				"DS:insert:DERIVE:120:0:U ".
				"DS:insert_failed:DERIVE:120:0:U ".
				"DS:drop:DERIVE:120:0:U ".
				"DS:early_drop:DERIVE:120:0:U ".
				"DS:icmp_error:DERIVE:120:0:U ".
				"DS:expect_new:DERIVE:120:0:U ".
				"DS:expect_create:DERIVE:120:0:U ".
				"DS:expect_delete:DERIVE:120:0:U ".
				RRAAVG.RRAMAX.RRAMIN);
			
			chmod(RRDPATH."/{$name}.rrd", RRDMOD);
			chgrp(RRDPATH."/{$name}.rrd", RRDGRP);
		}
		
		$str = 'N';
		foreach($data as $val)
			$str.=':'.$val;
		
		exec(RRDTOOL." update ".RRDPATH."/{$name}.rrd {$str}");
        }

        function graph($name, $start, $title) {
                header('Content-type: image/x-png');
                        passthru(RRDTOOL.' graph - --imgformat PNG --start '.$start.' --end -60 --title "'.$title.'" '.
                                '--rigid --base=1000 --height=240 --width=800 --alt-autoscale-max '.
                                '--vertical-label="calls" '.
                                "DEF:a=".RRDPATH."/{$name}.rrd:searched:AVERAGE ".
                                "DEF:b=".RRDPATH."/{$name}.rrd:found:AVERAGE ".
                                "DEF:c=".RRDPATH."/{$name}.rrd:new:AVERAGE ".
                                "DEF:d=".RRDPATH."/{$name}.rrd:invalid:AVERAGE ".
                                "DEF:e=".RRDPATH."/{$name}.rrd:ignore:AVERAGE ".
                                "DEF:f=".RRDPATH."/{$name}.rrd:delete:AVERAGE ".
                                "DEF:g=".RRDPATH."/{$name}.rrd:delete_list:AVERAGE ".
                                "DEF:h=".RRDPATH."/{$name}.rrd:insert:AVERAGE ".
                                "DEF:i=".RRDPATH."/{$name}.rrd:insert_failed:AVERAGE ".
                                "DEF:j=".RRDPATH."/{$name}.rrd:drop:AVERAGE ".
                                "DEF:k=".RRDPATH."/{$name}.rrd:early_drop:AVERAGE ".
                                "DEF:l=".RRDPATH."/{$name}.rrd:icmp_error:AVERAGE ".
                                "DEF:m=".RRDPATH."/{$name}.rrd:expect_new:AVERAGE ".
                                "DEF:n=".RRDPATH."/{$name}.rrd:expect_create:AVERAGE ".
                                "DEF:o=".RRDPATH."/{$name}.rrd:expect_delete:AVERAGE ".
                                'CDEF:ii=i,-1,* '.
                                'CDEF:jj=j,-1,* '.
                                'CDEF:kk=k,-1,* '.
                                'CDEF:ll=l,-1,* '.
                                'CDEF:mm=m,-1,* '.
                                'CDEF:nn=n,-1,* '.
                                'CDEF:oo=o,-1,* '.
                                'LINE1:a#FF0000:"searched      " GPRINT:a:LAST:"Current\:%8.0lf" GPRINT:a:AVERAGE:"Average\:%8.0lf" GPRINT:a:MAX:"Maximum\:%8.0lf\n" '.
                                'LINE1:b#FF00FF:"found         " GPRINT:b:LAST:"Current\:%8.0lf" GPRINT:b:AVERAGE:"Average\:%8.0lf" GPRINT:b:MAX:"Maximum\:%8.0lf\n" '.
                                'LINE1:c#0000FF:"new           " GPRINT:c:LAST:"Current\:%8.0lf" GPRINT:c:AVERAGE:"Average\:%8.0lf" GPRINT:c:MAX:"Maximum\:%8.0lf\n" '.
                                'LINE1:d#007FFF:"invalid       " GPRINT:d:LAST:"Current\:%8.0lf" GPRINT:d:AVERAGE:"Average\:%8.0lf" GPRINT:d:MAX:"Maximum\:%8.0lf\n" '.
                                'LINE1:e#00FFFF:"ignore        " GPRINT:e:LAST:"Current\:%8.0lf" GPRINT:e:AVERAGE:"Average\:%8.0lf" GPRINT:e:MAX:"Maximum\:%8.0lf\n" '.
                                'LINE1:f#00FF00:"delete        " GPRINT:e:LAST:"Current\:%8.0lf" GPRINT:e:AVERAGE:"Average\:%8.0lf" GPRINT:e:MAX:"Maximum\:%8.0lf\n" '.
                                'LINE1:g#FFFF00:"delete_list   " GPRINT:e:LAST:"Current\:%8.0lf" GPRINT:e:AVERAGE:"Average\:%8.0lf" GPRINT:e:MAX:"Maximum\:%8.0lf\n" '.
                                'LINE1:h#FF7F00:"insert        " GPRINT:e:LAST:"Current\:%8.0lf" GPRINT:e:AVERAGE:"Average\:%8.0lf" GPRINT:e:MAX:"Maximum\:%8.0lf\n" '.
                                'HRULE:0#000000:"-\n" '.
                                'LINE1:ii#FF0000:"insert_failed " GPRINT:i:LAST:"Current\:%8.0lf" GPRINT:i:AVERAGE:"Average\:%8.0lf" GPRINT:i:MAX:"Maximum\:%8.0lf\n" '.
                                'LINE1:jj#FF00FF:"drop          " GPRINT:j:LAST:"Current\:%8.0lf" GPRINT:j:AVERAGE:"Average\:%8.0lf" GPRINT:j:MAX:"Maximum\:%8.0lf\n" '.
                                'LINE1:kk#0000FF:"early_drop    " GPRINT:k:LAST:"Current\:%8.0lf" GPRINT:k:AVERAGE:"Average\:%8.0lf" GPRINT:k:MAX:"Maximum\:%8.0lf\n" '.
                                'LINE1:ll#007FFF:"icmp_errore   " GPRINT:l:LAST:"Current\:%8.0lf" GPRINT:l:AVERAGE:"Average\:%8.0lf" GPRINT:l:MAX:"Maximum\:%8.0lf\n" '.
                                'LINE1:mm#00FFFF:"expect_new    " GPRINT:m:LAST:"Current\:%8.0lf" GPRINT:m:AVERAGE:"Average\:%8.0lf" GPRINT:m:MAX:"Maximum\:%8.0lf\n" '.
                                'LINE1:nn#00FF00:"expect_create " GPRINT:n:LAST:"Current\:%8.0lf" GPRINT:n:AVERAGE:"Average\:%8.0lf" GPRINT:n:MAX:"Maximum\:%8.0lf\n" '.
                                'LINE1:oo#FFFF00:"expect_delete " GPRINT:o:LAST:"Current\:%8.0lf" GPRINT:o:AVERAGE:"Average\:%8.0lf" GPRINT:o:MAX:"Maximum\:%8.0lf\n" '.
                                '');
        }
}
?>
