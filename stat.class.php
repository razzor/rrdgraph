<?php

class stat {

    function get_cpu() {
	$stat = file('/proc/stat');
	foreach ($stat as $line) {
	    if (strpos($line, 'cpu') !== false) {
		$parts = preg_split("/[\s:]+/", trim($line));
		$id = array_shift($parts);
    		$cpu[$id] = $parts;
	    }
	}
	return $cpu;
    }
    
    function get_sys() {
	$stat = file('/proc/stat');
	foreach ($stat as $line) {
	    if (strpos($line, 'intr') !== false) {
		$parts = preg_split("/[\s:]+/", trim($line));
		$proc[0] = $parts[1];

	    } else if (strpos($line, 'ctxt') !== false) {
		$parts = preg_split("/[\s:]+/", trim($line));
		$proc[1] = $parts[1];
	    }
	}
	$vmstat = file('/proc/vmstat');
	foreach ($vmstat as $line) {
	    if (strpos($line, 'pgfault ') !== false) {
		$parts = preg_split("/[\s:]+/", trim($line));
		$proc[2] = $parts[1];	    
    	    }
	}
	return $proc;
    }

    function get_load() {
	$loadavg = file('/proc/loadavg');
	$load = preg_split("/[\s:]+/", trim($loadavg[0]));
	array_pop($load);
	array_pop($load);
	return $load;
    }

    function get_mem() {
	$meminfo = file('/proc/meminfo');
	foreach ($meminfo as $line) {
	    if (strpos($line, 'MemTotal:') !== false) {
		$parts = preg_split("/[\s:]+/", trim($line));
    		$mem[0][0] = $parts[1];

	    } else if (strpos($line, 'MemFree:') !== false) {
		$parts = preg_split("/[\s:]+/", trim($line));
    		$mem[0][1] = $parts[1];

	    } else if (strpos($line, 'Buffers:') !== false) {
		$parts = preg_split("/[\s:]+/", trim($line));
    		$mem[0][2] = $parts[1];

	    } else if (strpos($line, 'SwapCached:') !== false) {
		// nothing
	    
	    } else if (strpos($line, 'Cached:') !== false) {
		$parts = preg_split("/[\s:]+/", trim($line));
    		$mem[0][3] = $parts[1];

	    } else if (strpos($line, 'SwapTotal:') !== false) {
		$parts = preg_split("/[\s:]+/", trim($line));
    		$mem[1][0] = $parts[1];

	    } else if (strpos($line, 'SwapFree:') !== false) {
		$parts = preg_split("/[\s:]+/", trim($line));
    		$mem[1][1] = $parts[1];
	    }
	}
	return $mem;
    }

    function get_uptime() {
        $uptime = file('/proc/uptime');
	$up = preg_split("/[\s:]+/", trim($uptime[0]));
	return $up;
    }

    function get_interfaces() {
	$netdev = file('/proc/net/dev');
	foreach ($netdev as $line) {
	    if (strpos($line, ':') !== false) {
		$parts = preg_split("/[\s:]+/", trim($line));
		$devs[$parts[0]][0] = $parts[1];
		$devs[$parts[0]][1] = $parts[9];
	    }

        }
	return $devs;
    }    
    
    function get_disks() {
	$diskstats = file('/proc/diskstats');
	$disks = array();
	foreach ($diskstats as $line) {
	    $parts = preg_split("/[\s:]+/", trim($line));
	    $disks[$parts[2]][0] = $parts[3];
	    $disks[$parts[2]][1] = $parts[4];
	    $disks[$parts[2]][2] = $parts[5];
	    $disks[$parts[2]][3] = $parts[6];
	}
	exec('/bin/df -P', $fsstat);
	foreach ($fsstat as $line) {
	    if (strpos($line, '/dev/') === 0) {
		$parts = preg_split("/[\s:]+/", trim($line));
		$name = str_replace('/dev/', '', $parts[0]);
		$fs[$name][0] = $parts[1];
		$fs[$name][1] = $parts[2];
		$fs[$name][2] = $parts[3];
		$fs[$name][3] = $disks[$name];
	    }
	}
	return $fs;
    }

    function get_mysql($host, $user, $pass) {
	$dbh = mysql_connect($host, $user, $pass);
	$res = mysql_query('SHOW STATUS', $dbh);
	while ($row = mysql_fetch_assoc($res)) {
	    if (strpos($row['Variable_name'], "Questions") !== false)
		$mysql[0] = $row['Value'];
	
	    else if (strpos($row['Variable_name'], "Threads_connected") !== false)
		$mysql[1] = $row['Value'];
	}
	mysql_close($dbh);
	return $mysql;
    }

    function get_httpd($host) {
	$status = file($host);
	foreach($status as $line) {
	    if (strpos($line, 'Total Accesses') !== false) {
		$parts = preg_split("/[\s:]+/", trim($line));
		$httpd[0] = $parts[2];

	    } else if (strpos($line, 'Total kBytes') !== false) {
		$parts = preg_split("/[\s:]+/", trim($line));
		$httpd[1] = $parts[2];
	
	    } else if (strpos($line, 'BusyWorkers') !== false) {
		$parts = preg_split("/[\s:]+/", trim($line));
		$httpd[2] = $parts[1];
	
	    } else if (strpos($line, 'IdleWorkers') !== false) {
		$parts = preg_split("/[\s:]+/", trim($line));
		$httpd[3] = $parts[1];
	    }
	}
	return $httpd;
    }

    function get_named($statfile) {
	if (file_exists($statfile))
	    unlink($statfile);

	exec('/usr/sbin/rndc stats');

	$cnt = 100;
	while (!file_exists($statfile) && ($i-- > 0)) 
	    usleep(100000);

	$stats = file($statfile);
	foreach ($stats as $line) {
	    if (strpos($line, 'success') !== false) {
		$parts = preg_split("/[\s:]+/", trim($line));
		$named[0] = $parts[1];

	    } elseif (strpos($line, 'referral') !== false) {
		$parts = preg_split("/[\s:]+/", trim($line));
		$named[1] = $parts[1];

	    } elseif (strpos($line, 'nxrrset') !== false) {
		$parts = preg_split("/[\s:]+/", trim($line));
		$named[2] = $parts[1];

	    } elseif (strpos($line, 'nxdomain') !== false) {
		$parts = preg_split("/[\s:]+/", trim($line));
		$named[3] = $parts[1];

	    } elseif (strpos($line, 'recursion') !== false) {
		$parts = preg_split("/[\s:]+/", trim($line));
		$named[4] = $parts[1];

	    } elseif (strpos($line, 'failure') !== false) {
		$parts = preg_split("/[\s:]+/", trim($line));
		$named[5] = $parts[1];
	    }
	}
	return $named;
    }

    function get_squid($host, $comunity) {
	$cmd = "/usr/bin/snmpget -v1 -c {$comunity} {$host} ".
		".1.3.6.1.4.1.3495.1.3.2.1.1 .1.3.6.1.4.1.3495.1.3.2.1.2 ".
		".1.3.6.1.4.1.3495.1.3.2.1.3 .1.3.6.1.4.1.3495.1.3.2.1.4 ".
		".1.3.6.1.4.1.3495.1.3.2.1.5 .1.3.6.1.4.1.3495.1.3.2.1.10 ".
		".1.3.6.1.4.1.3495.1.3.2.1.11 .1.3.6.1.4.1.3495.1.3.2.1.12 ".
		".1.3.6.1.4.1.3495.1.3.2.1.13 .1.3.6.1.4.1.3495.1.3.2.1.14 ".
		"-O qs";
		
	exec($cmd, $poll);
	foreach ($poll as $line) {
	    $parts = preg_split("/[\s:]+/", trim($line));
	    $squid[] = $parts[1];
	}
	return $squid;    
    }

    function get_conntrack() {
        $retval = array();
        $retval['total'] = 0;
        $retval['assured'] = 0;
        $retval['unreplied'] = 0;
        $retval['tcp'] = 0;
        $retval['udp'] = 0;
        $retval['unknown'] = 0;
        $retval['stat']['NONE'] = 0;
        $retval['stat']['SYN_SENT'] = 0;
        $retval['stat']['SYN_RECV'] = 0;
        $retval['stat']['ESTABLISHED'] = 0;
        $retval['stat']['FIN_WAIT'] = 0;
        $retval['stat']['CLOSE_WAIT'] = 0;
        $retval['stat']['LAST_ACK'] = 0;
        $retval['stat']['TIME_WAIT'] = 0;
        $retval['stat']['CLOSE'] = 0;
        $retval['stat']['LISTEN'] = 0;
        $retval['local'] = 0;
        $retval['nated'] = 0;

        $max = file('/proc/sys/net/ipv4/ip_conntrack_max');
        $retval['max'] = (int)$max[0];

        $conn = file('/proc/net/ip_conntrack');
        foreach ($conn as $line) {
            $flags = 0x0;

            $retval['total']++;
            $parts = preg_split("/[\s:]+/", trim($line));

            if (strpos($line, '[ASSURED]') !== false) {
                $retval['assured']++;
            }

            if (strpos($line, '[UNREPLIED]') !== false) {
                $retval['unreplied']++;
                $flags |= 0x1;
            }

            if (strpos($line, 'tcp') !== false) {
                $retval['tcp']++;
                $retval['stat'][$parts[3]]++;

                if (!($flags & 0x1)) {
                    // dst1 != src2 || src1 != dst2
                    if (substr($parts[5], 4) != substr($parts[10], 4) || substr($parts[4], 4) != substr($parts[11], 4))
                        $retval['nated']++;
                    else
                        $retval['local']++;
	        }

            } else if (strpos($line, 'udp') !== false) {
                $retval['udp']++;

                if (!($flags & 0x1)) {
                    // dst1 != src2 || src1 != dst2
                    if (substr($parts[4], 4) != substr($parts[9], 4) || substr($parts[3], 4) != substr($parts[10], 4))
                        $retval['nated']++;
                    else
                        $retval['local']++;
                }

            } else {
                $retval['unknown']++;
            }
        }
        return $retval;
    }
    
    function get_netstat($file) {
	$stat = file($file);
	$retval = preg_split("/[\s:]+/", trim($stat[1]));
	foreach ($retval as $key => $val)
		$retval[$key] = hexdec($val);
	return $retval;    
    }
}

?>

