#!/usr/bin/php 
<?php
define('RRDTOOL', '/usr/bin/rrdtool');
define('RRDPATH', '/home/httpd/rrd');
define('RRDMOD', 0640);
define('RRDGRP', 'httpd');

require_once("/etc/rrdtool/stat.class.php");
require_once("/etc/rrdtool/rrd.class.php");


function get_all() {
	$arr['cpu'] = stat::get_cpu();
	$arr['sys'] = stat::get_sys();
	$arr['load'] = stat::get_load();
	$arr['mem'] = stat::get_mem();
	$arr['up'] = stat::get_uptime();
	$arr['iface'] = stat::get_interfaces();
	$arr['disks'] = stat::get_disks();
	$arr['mysql'] = stat::get_mysql('localhost', 'status', '');
	$arr['httpd'] = stat::get_httpd('http://127.0.0.1/server-status?auto');
	$arr['named'] = stat::get_named('/var/lib/named/chroot/var/run/named/named.stats');
	$arr['squid'] = stat::get_squid('localhost:3401', 'public');
	$arr['conntrack'] = stat::get_conntrack();
	$arr['rtstat'] = stat::get_netstat('/proc/net/stat/rt_cache');
	$arr['ctstat'] = stat::get_netstat('/proc/net/stat/ip_conntrack');
	return $arr;
}


function parse_all($arr) {
	foreach ($arr['cpu'] as $name => $cpu)
		if ($name != 'cpu')
			CpuRRD::update($name, $cpu);
	
	ProcRRD::update("proc", $arr['sys']);
	LavgRRD::update("load", $arr['load']);
	MemRRD::update("mem", $arr['mem'][0]);
	SwapRRD::update("swap", $arr['mem'][1]);
	UpRRD::update('uptime', $arr['up']);
	
	foreach ($arr['iface'] as $name => $iface)
		IfaceRRD::update("if_".$name, $iface[0], $iface[1]);
	
	
	foreach ($arr['disks'] as $name => $disk) {
		FsRRD::update('fs_'.$name, $disk);
		DiskRRD::update('disk_'.$name, $disk[3]);
	}
	
	if (isset($arr['mysql']))
		MysqlRRD::update("mysql", $arr['mysql']);
	
	if (isset($arr['httpd']))
		HttpdRRD::update("httpd", $arr['httpd']);
	
	if (isset($arr['named']))
		NamedRRD::update("named", $arr['named']);
	
	if (isset($arr['squid']))
		SquidRRD::update("squid", $arr['squid']);
	
	if (isset($arr['conntrack'])) {
		ConntrackRRD::update("conntrack", $arr['conntrack']);
		TcpRRD::update("tcp", $arr['conntrack']['stat']);
	}

	if (isset($arr['rtstat']))
		RtStatRRD::update("rtstat", $arr['rtstat']);

	if (isset($arr['ctstat']))
		CtStatRRD::update("ctstat", $arr['ctstat']);
}


function request_all($host, $port) {
	$handle = fsockopen($host, $port, $errno, $errstr, 10);
	if (!$handle) {
		echo "$errstr ($errno)";
		die();
	}
	$buffer = "";
	while (!feof($handle))
		$buffer .= fgets($handle, 4096);
	fclose($handle);
	
	return unserialize(trim($buffer));
}


if (!isset($_SERVER['argv'][1]))
	$_SERVER['argv'][1] = "local";
    
switch ($_SERVER['argv'][1]) {
	case 'local':	parse_all(get_all());
			break;
	
	case 'server':	echo serialize(get_all())."\n";
			break;
	
	case 'client':	if (!isset($_SERVER['argv'][2]) || !isset($_SERVER['argv'][3]))
				break;
			
			parse_all(request_all($_SERVER['argv'][2], $_SERVER['argv'][3]));
			break;
	
	case 'test':	print_r(get_all());
			break;
}    
?>