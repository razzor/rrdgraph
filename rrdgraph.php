<?php

define('RRDTOOL', '/usr/bin/rrdtool');
define('RRDPATH', '/home/httpd/rrd');
define('RRDMOD', 0640);
define('RRDGRP', 'httpd');

require_once("/etc/rrdtool/rrd.class.php");

$graphs = array('cpu0' => 'CPU Usage',
		'proc' => 'Interrupts / Context Switchs',
		'load' => 'Load Average',
		'mem' => 'Memory Usage',
		'swap' => 'Swap Usage',
		'uptime' => 'Uptime (days)',
		'hr',
		'if_br0' => 'Traffic br0 - internal bridge',
		'if_eth0' => 'Traffic eth0 - 100BaseT - bridged',
		'if_wlan0' => 'Traffic wlan0 - 802.11b - bridged',
		'if_ppp0' => 'Traffic ppp0 - 768/128 TDSL',
		'if_ppp1' => 'Traffic ppp1 - 115.2kbaud dect-link',
		'if_tap0' => 'Traffic tap0 - layer2 tunnel - bridged (seppel)',
		'if_tap2' => 'Traffic tap2 - openvpn tunnel (xittix) - routed',
		'if_tap3' => 'Traffic tap3 - openvpn tunnel (server) - routed',
		'if_tap5' => 'Traffic tap5 - layer2 tunnel - bridged (roadwarrior)',
		'if_sixxs' => 'Traffic sixxs - ipv6 tunnel',
		'if_hpot0' => 'Traffic hpot0 - to/from honeypot',
		'hr',
		'conntrack1' => 'IP Connections (overview)',
		'conntrack2' => 'IP Connections (protocols)',
		'tcp' => 'TCP Connection States',
		'ctstat' => 'Conntrack Statistics',
		'rtstat1' => 'Routing Cache Statistics',
		'rtstat2' => 'Routing Cache Garbage Collector',
		'hr',
		'mysql' => 'MySQL Usage',
		'httpd' => 'Apache Usage',
		'squid1' => 'Squid Proxy Traffic Usage',
		'squid2' => 'Squid Proxy Requests/Hits',
		'named' => 'DNS Usage',
		'hr',
		'fs_hda3' => 'Filesystem /',
		'disk_hda3' => 'read/write /',
		'fs_hda4' => 'Filesystem /home',
		'disk_hda4' => 'read/write /home',
		'fs_hdc1' => 'Filesystem /mnt/data',
		'disk_hdc1' => 'read/write /mnt/data',
		);

$views = array(	1 => -3600 * 6,
		2 => -3600 * 24,
		3 => -3600 * 24 * 7,
		4 => -3600 * 24 * 28,
		5 => -3600 * 24 * 365,
		);

function html_overview($view = 1) {
	global $graphs;
	echo "<html><head><title>RRDgraph Overview</title></head><body>\n";
	foreach ($graphs as $graph => $text) {
		if ($text != 'hr') {
		    echo "<a href=\"{$_SERVER['PHP_SELF']}?show={$graph}\">";
    		    echo "<img src=\"{$_SERVER['PHP_SELF']}?graph={$graph}&view={$view}\" border=\"0\">";
		    echo "</a><br><br>\n";
		
		} else {
		    echo "<hr><br>";
		}
	}
	echo "</body></html>\n";
}

function html_detail($graph) {
	global $graphs, $views;
	echo "<html><head><title>{$graphs[$graph]}</title></head><body>\n";
	foreach ($views as $view => $start) {
		echo "<img src=\"{$_SERVER['PHP_SELF']}?graph={$graph}&view={$view}\">";
		echo "<br><br>\n";
	}
	echo "</body></html>\n";
}

function img_graph($graph, $view) {
	global $graphs, $views;

	$start = (isset($views[$view])) ? $views[$view] : $views[1];
	switch ($graph) {
		case 'proc':
			ProcRRD::graph($graph, $start, $graphs[$graph]);
			break;

		case 'load':
			LavgRRD::graph($graph, $start, ($view == 1), $graphs[$graph]);
			break;

		case 'mem':
			MemRRD::graph($graph, $start, $graphs[$graph]);
			break;

		case 'swap':
			SwapRRD::graph($graph, $start, $graphs[$graph]);
			break;

		case 'mysql':
			MysqlRRD::graph($graph, $start, $graphs[$graph]);
			break;

		case 'httpd':
			HttpdRRD::graph($graph, $start, $graphs[$graph]);
			break;

		case 'named':
			NamedRRD::graph($graph, $start, $graphs[$graph]);
			break;

		case 'uptime':
			UpRRD::graph($graph, $start, $graphs[$graph]);
			break;

		case 'squid1':
			SquidRRD::graph1(substr($graph, 0, strlen('squid')), $start, $graphs[$graph]);
			break;

		case 'squid2':
			SquidRRD::graph2(substr($graph, 0, strlen('squid')), $start, $graphs[$graph]);
			break;

		case 'conntrack1':
			ConntrackRRD::graph1(substr($graph, 0, strlen('conntrack')), $start, ($view == 1), $graphs[$graph]);
			break;

		case 'conntrack2':
			ConntrackRRD::graph2(substr($graph, 0, strlen('conntrack')), $start, $graphs[$graph]);
			break;

		case 'tcp':
			TcpRRD::graph($graph, $start, $graphs[$graph]);
			break;

		case 'rtstat1':
			RtStatRRD::graph1(substr($graph, 0, strlen('rtstat')), $start, $graphs[$graph]);
			break;

		case 'rtstat2':
			RtStatRRD::graph2(substr($graph, 0, strlen('rtstat')), $start, $graphs[$graph]);
			break;

		case 'ctstat':
			CtStatRRD::graph($graph, $start, $graphs[$graph]);
			break;

		default:
			if (strpos($graph, 'if_') !== false)
				IfaceRRD::graph($graph, $start, ($view == 1), $graphs[$graph]);
				
			else if (strpos($graph, 'fs_') !== false)
				FsRRD::graph($graph, $start, $graphs[$graph]);
			
			else if (strpos($graph, 'disk_') !== false)
				DiskRRD::graph($graph, $start, $graphs[$graph]);

			else if (strpos($graph, 'cpu') !== false)
				CpuRRD::graph($graph, $start, ($view == 1), $graphs[$graph]);
			
			break;
	}
}

if (isset($_GET['show'])) {
	html_detail($_GET['show']);

} else if (isset($_GET['graph'])) {
	$view = (isset($_GET['view'])) ? $_GET['view'] : 1;
	img_graph($_GET['graph'], $view);

} else {
	html_overview();
}

?>
